var express = require('express');
var templateEngine = require('ejs');
var path = require('path');
var app = express();

app.set('views', path.join(__dirname, 'application', 'templates'));
app.engine('html', templateEngine.renderFile);
app.set('view engine', 'html');

app.use('/demo', express.static(path.join(__dirname, 'application', 'demo')));

app.use(function(req, res, next) {
    res.status(200).send('404 not found');
});

app.listen(8006, function ()
{
    console.log("server is up and running on port 8006");
});
