angular.module('rssFeed', []);
angular.module('rssFeed').factory('newsTickerTemplates',[function() {
    var api = {};

    api.mainTemplate = '<div class="newsTickerWrapper">' +
        '<div class="tickerTitle">' +
            '<div class="title">{{metaData.title}}</div>' +
        '</div>' +
        '<div class="tickerContent">' +
            '<div class="loading_feed"><div class="loading_text">Loading . . .</div></div>' +
            '<div class="rssFeed" ng-repeat="feed in feeds track by $index"><span>{{feed.title}}</span>' +
            '<a href="{{feed.link}}" target="_blank" class="readmore" style="background: {{config.styles.titleBackgroundColor}}">Read More</a>' +
            '<div style="clear: both"></div>' +
            '</div>' +
        '</div>' +
        '</div>';

    return api;
}]);

angular.module('rssFeed').directive("intelliRssFeed",['newsTickerTemplates', '$http', '$timeout', function(newsTickerTemplates, $http, $timeout) {
    var directive = {};
    directive.restrict = 'E';
    directive.scope = {
        config: '='
    };
    directive.template = newsTickerTemplates.mainTemplate;
    directive.require = ['intelliRssFeed'];
    directive.link = function($scope, $element) {
        var $jElement = $($element);
        var jNewsTickerWrapper = $jElement.find('.newsTickerWrapper');
        var jTickerTitle = jNewsTickerWrapper.find('.tickerTitle');
        var jTickerContent = jNewsTickerWrapper.find('.tickerContent');
        function init() {
            processStyling();
        };
        function processStyling() {
            jTickerTitle.css('background-color', $scope.config.styles.titleBackgroundColor);
            jNewsTickerWrapper.css('border-color', $scope.config.styles.titleBackgroundColor);
            jNewsTickerWrapper.height();
            jTickerContent.css('height', $scope.metaData.widgetHeight + 'px');
            jTickerTitle.find('div.title').css('color', $scope.config.styles.titleColor);
        };
        init();
    };
    directive.controller = function($scope, $element, $attrs) {
        var thisController = this;
        var $jElement = $($element);
        var jNewsTickerWrapper = $jElement.find('.newsTickerWrapper');
        var jTickerTitle = jNewsTickerWrapper.find('.tickerTitle');
        var jTickerContent = jNewsTickerWrapper.find('.tickerContent');
        this.init = function() {
            this.validateAndInitConfiguration();
            this.initRssReader();
        };
        $attrs.$observe('feedUrl',function(newVal, oldVal) {
            if (oldVal != newVal) {
                thisController.init();
            }
        });
        $attrs.$observe('feedTitle',function(newVal, oldVal) {
            if (oldVal != newVal) {
                $scope.metaData.title = newVal;
            }
        });
        this.initRssReader = function() {
            $scope.feeds = [];
            jTickerContent.find('.loading_feed').show();
            var rssFeed = this.getRssFeed($scope.metaData.feedUrl, $scope.config.feedCount);
            rssFeed.then(function(data) {
                if (data.data.responseData) {
                    var feeds = data.data.responseData.feed.entries;
                }
                else {
                    $scope.feeds = [];
                }
                $scope.feeds = feeds;
                $timeout(function() {
                    jTickerContent.find('.loading_feed').hide();
                });
            });
        };
        this.getRssFeed = function(url, count) {
            return $http.jsonp('//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=' + count + '&callback=JSON_CALLBACK&q=' + encodeURIComponent(url));
        };
        this.validateAndInitConfiguration = function() {
            $scope.metaData = {};
            $scope.metaData.feedUrl = $attrs['feedUrl'];
            $scope.metaData.title = $attrs['feedTitle'];
            $scope.metaData.widgetHeight = $attrs['widgetHeight'];
            if (!$scope.metaData.widgetHeight) {
                $scope.metaData.widgetHeight = 500;
            }
            if (!$attrs['feedTitle']) {
                $scope.metaData.title = 'RSS Feed';
            }
            if (!$scope.config) {
                $scope.config = {};
            }
            if (!$scope.config.styles) {
                $scope.config.styles = {};
            }
            if (!$scope.config.styles.titleBackgroundColor) {
                $scope.config.styles.titleBackgroundColor = '#1fb5ad';
            }
            if (!$scope.config.styles.titleColor) {
                $scope.config.styles.titleColor = '#fff';
            }
            if (!$scope.config.feedCount) {
                $scope.config.feedCount = 50;
            }
        };

        this.init();
    };
    return directive;
}]);