var sample = angular.module('sample', ['ui.router', 'rssFeed']);

sample.config(['$stateProvider', '$urlRouterProvider', '$sceProvider', function ($stateProvider, $urlRouterProvider, $sceProvider) {
    $sceProvider.enabled(false);
    $stateProvider.state('home',
        {
            url: '/home',
            templateUrl: './demoapp/templates/home.html',
            controller: 'homeController'
        });
    $urlRouterProvider.otherwise('/home');
}]);

sample.controller('homeController',['$scope','$timeout', function($scope) {
    $scope.url1 = 'http://feeds.feedburner.com/TEDTalks_video';
    $scope.url2 = 'http://feeds.feedburner.com/TechCrunch';
    $scope.url3 = 'http://feeds2.feedburner.com/Mashable';
    $scope.url4 = 'http://feeds.huffingtonpost.com/huffingtonpost/raw_feed';
    $scope.title1 = 'Ted Talks';
    $scope.title2 = 'Tech Crunch';
    $scope.title3 = 'Mashable';
    $scope.title4 = 'Huffington Post';

    $scope.sampleConfiguration1 = {};
    $scope.sampleConfiguration1.styles = {};
    $scope.sampleConfiguration1.styles.titleBackgroundColor = '#d3ac23';

    $scope.sampleConfiguration3 = {};
    $scope.sampleConfiguration3.styles = {};
    $scope.sampleConfiguration3.styles.titleBackgroundColor = '#f44a56';

    $scope.sampleConfiguration4 = {};
    $scope.sampleConfiguration4.styles = {};
    $scope.sampleConfiguration4.styles.titleBackgroundColor = '#f46e27';
}]);